﻿using System;
using System.Collections.Generic;
using Bogus;
using CarBuilderExample.Models;

namespace CarBuilderExampleTests.Helpers
{
    public class FeatureFaker
    {
        private Faker<Feature> featureGenerator;

        public FeatureFaker()
        {
            featureGenerator = new Faker<Feature>()
                .RuleFor(fe => fe.BasePriceModifier, f => f.Random.Float())
                .RuleFor(fe => fe.FeatureName, f => f.Random.Word());
        }

        public Feature GetFeature() {
            return featureGenerator.Generate();
        }

        public List<Feature> GetFeatures() {
            List<Feature> features = new List<Feature>();

            Random random = new Random();
            int featuresCount = random.Next(5, 21);

            for(int i = 0; i < featuresCount; i++) {
                Feature feature = this.GetFeature();
                features.Add(feature);
            }

            return features;
        }
    }
}
