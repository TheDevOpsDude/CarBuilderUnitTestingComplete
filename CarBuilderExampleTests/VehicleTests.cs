using System;
using System.Collections.Generic;
using CarBuilderExample.Models;
using CarBuilderExampleTests.Helpers;
using Xunit;

namespace CarBuilderExampleTests
{
    public class VehicleTests
    {
        // A newly created vehicle should not be a null object
        [Fact]
        public void TestConstructVehicle()
        {
            Vehicle vehicle = new Vehicle();
            Assert.NotNull(vehicle);
        }

        // The price calculator should set vehicle price as base + feature costs
        [Fact]
        public void TestVehicleCalculateTotalPrice() {
        // Randomizing a list of features and the base price
        List<Feature> features = new FeatureFaker().GetFeatures();
            double basePrice = new Random().Next(5000, 5000000);

            // Calculating expected total price
            double expectedFinalPrice = basePrice;

            foreach(var feature in features) {
                expectedFinalPrice += feature.BasePriceModifier;
            }

            // Creating a vehicle with our features and base price
            Vehicle vehicle = new Vehicle() {
                Features = features,
                BasePrice = basePrice
            };

            // Calling method in question
            vehicle.CalculateTotalPrice();

            // Assertion
            Assert.Equal(vehicle.TotalPrice, expectedFinalPrice);
        }
    }
}
