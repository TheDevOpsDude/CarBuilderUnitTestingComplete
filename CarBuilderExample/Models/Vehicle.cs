﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CarBuilderExample.Models
{
    public class Vehicle
    {
        public Engine Engine { get; set; }
        public Body Body { get; set; }
        public List<Feature> Features { get; set; }
        public double BasePrice { get; set; }
        public double FuelCapacity { get; set; }
        public double TotalPrice { get; private set; }

        public void CalculateTotalPrice() {
            double totalPrice = BasePrice;

            // Method one:
            foreach (var feature in Features)
            {
                totalPrice += feature.BasePriceModifier;
            }

            //// Method two:
            //double featuresTotal = Features.Sum(f => f.BasePriceModifier);
            //totalPrice += featuresTotal;

            TotalPrice = totalPrice;
        }

        public Vehicle()
        {
        }
    }
}
