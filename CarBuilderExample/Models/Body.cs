﻿using System;
namespace CarBuilderExample.Models
{
    public class Body
    {
        public string BodyType { get; set; }
        public int DoorCount { get; set; }

        public Body()
        {
        }
    }
}
