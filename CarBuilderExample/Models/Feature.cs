﻿using System;
namespace CarBuilderExample.Models
{
    public class Feature
    {
        public string FeatureName { get; set; }
        public double BasePriceModifier { get; set; }

        public Feature()
        {
        }
    }
}
